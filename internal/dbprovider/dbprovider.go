package dbprovider

import (
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // postgres driver
	"gitlab.com/ffSaschaGff/vpn-checker/internal/settings"
)

const (
	connLifeTimeMin = 5
	maxCons         = 5
)

// NewConn - create database object.
func NewConn() *sqlx.DB {
	database, err := sqlx.Connect("postgres", connStr())
	if err != nil {
		log.Panic(err)
	}
	database.SetMaxOpenConns(maxCons)
	database.SetConnMaxIdleTime(time.Minute * time.Duration(connLifeTimeMin))
	database.SetConnMaxLifetime(time.Minute * time.Duration(connLifeTimeMin))

	return database
}

func connStr() string {
	settings := settings.AppSettings

	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
		settings.DBHost, settings.DBLogin, settings.DBPassword, settings.DBName)
}
