package vpn

import (
	"context"
	"log"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type FreeOpenVpn struct {
	url string
}

type IPSpeedVpn struct {
	url string
}

func NewFreeOpenVpn() FreeOpenVpn {
	return FreeOpenVpn{
		url: "https://www.freeopenvpn.org",
	}
}

func NewIPSpeedVPN() IPSpeedVpn {
	return IPSpeedVpn{
		url: "https://ipspeed.info",
	}
}

func (freeOpenVpn FreeOpenVpn) Next() []*OVPN {
	vpns := []*OVPN{}

	rootDoc, err := getDocumentByLink(freeOpenVpn.url)
	if err != nil {
		return vpns
	}

	urlMatcher := regexp.MustCompile(`private\.php\?cntid=.+`)
	rootDoc.Find("a").Each(func(i int, selection *goquery.Selection) {
		link, exist := selection.Attr("href")
		if !exist {
			return
		}
		if !urlMatcher.Match([]byte(link)) {
			return
		}
		if strings.Contains(link, "Russia") {
			return
		}

		vpns = append(vpns, freeOpenVpn.getVPNsByLink(link)...)
	})

	checkVPNs(vpns)

	return vpns
}

func (freeOpenVpn FreeOpenVpn) getVPNsByLink(rootLink string) []*OVPN {
	vpns := []*OVPN{}

	rootDoc, err := getDocumentByLink(freeOpenVpn.url + "/" + rootLink)
	if err != nil {
		return vpns
	}

	urlMatcher := regexp.MustCompile(`.+\.ovpn$`)
	rootDoc.Find("a").Each(func(i int, selection *goquery.Selection) {
		link, exist := selection.Attr("href")
		if !exist {
			return
		}
		if !urlMatcher.Match([]byte(link)) {
			return
		}

		vpn := OVPN{
			URL:    freeOpenVpn.url + link,
			Name:   selection.Text(),
			Status: NotChecked,
		}
		vpns = append(vpns, &vpn)
	})

	return vpns
}

func (ipSpeedVpn IPSpeedVpn) Next() []*OVPN {
	vpns := []*OVPN{}
	limit := 20

	rootDoc, err := getDocumentByLink(ipSpeedVpn.url + "/freevpn_openvpn.php?language=en")
	if err != nil {
		return vpns
	}

	cells := rootDoc.Find(".list")
	for index := 0; index < len(cells.Nodes)/4; index++ {
		country := cells.Nodes[index*4].FirstChild.Data
		if country == "Russian Federation" {
			continue
		}

		addr := cells.Nodes[1+index*4].FirstChild
	outer:
		for _, addrAtribute := range addr.Attr {
			if addrAtribute.Key == "href" {
				vpn := OVPN{
					URL:    ipSpeedVpn.url + addrAtribute.Val,
					Name:   addr.FirstChild.Data,
					Status: NotChecked,
				}
				vpns = append(vpns, &vpn)

				continue outer
			}
		}

		if len(vpns) > limit {
			break
		}
	}

	checkVPNs(vpns)

	return vpns
}

func checkVPNs(vpnList []*OVPN) {
	waitGroup := sync.WaitGroup{}

	for _, vpn := range vpnList {
		waitGroup.Add(1)
		go func(vpn *OVPN, wg *sync.WaitGroup) {
			defer wg.Done()
			vpn.Check()
		}(vpn, &waitGroup)
	}

	waitGroup.Wait()
}

func getDocumentByLink(url string) (*goquery.Document, error) {
	timeoutCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	req, err := http.NewRequestWithContext(timeoutCtx, http.MethodGet, url, nil)
	if err != nil {
		log.Println(err.Error())

		return nil, err
	}

	hc := http.Client{}
	res, err := hc.Do(req)
	if err != nil {
		log.Println(err.Error())

		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		log.Printf("status code error: %d %s\n", res.StatusCode, res.Status)

		return nil, err
	}

	rootDoc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Println(err.Error())

		return nil, err
	}

	return rootDoc, nil
}
