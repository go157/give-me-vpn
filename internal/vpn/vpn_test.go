package vpn_test

import (
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/ffSaschaGff/vpn-checker/internal/mock_vpn"
	"gitlab.com/ffSaschaGff/vpn-checker/internal/repository"
	"gitlab.com/ffSaschaGff/vpn-checker/internal/vpn"
)

type fields struct {
	repo    vpn.Repo
	sources []vpn.Source
	timer   vpn.Timer
}

func TestReadRandomVPN(t *testing.T) {
	t.Parallel()

	type result struct {
		vpn vpn.OVPN
		err error
	}

	type test struct {
		name           string
		fields         fields
		expectedResult result
	}

	resultVPN := vpn.OVPN{
		URL:    "http://example/ovpn.ovpn",
		Name:   "sone vpn",
		Status: vpn.Work,
	}
	repoVPN := repository.OVPN{
		URL:    resultVPN.URL,
		Name:   resultVPN.Name,
		Status: int(resultVPN.Status),
		ID:     1,
		TS:     time.Now().Unix(),
	}

	tests := []test{
		{
			name: "valid get vpn",
			fields: fields{
				repo: func() vpn.Repo {
					m := mock_vpn.NewMockRepo(gomock.NewController(t))
					m.EXPECT().ReadRandomVPN(int(vpn.Work)).Return(repoVPN, nil)
					return m
				}(),
				sources: func() []vpn.Source {
					m := mock_vpn.NewMockSource(gomock.NewController(t))
					return []vpn.Source{m}
				}(),
			},
			expectedResult: result{
				vpn: resultVPN,
				err: nil,
			},
		},
	}

	for _, oneTest := range tests {
		oneTest := oneTest
		t.Run(t.Name(), func(t *testing.T) {
			t.Parallel()

			app := vpn.NewApp(oneTest.fields.repo, oneTest.fields.sources, oneTest.fields.timer)
			vpn, err := app.ReadRandomVPN()

			require.Equal(t, vpn, oneTest.expectedResult.vpn)
			require.Equal(t, err, oneTest.expectedResult.err)
		})
	}
}

func TestUpdateVPN(t *testing.T) {
	t.Parallel()

	type test struct {
		name        string
		fields      fields
		expectedErr error
	}

	resultVPN := vpn.OVPN{
		URL:    "http://example/ovpn.ovpn",
		Name:   "sone vpn",
		Status: vpn.Work,
	}
	timeWrite := time.Now()
	repoVPN := repository.OVPN{
		URL:    resultVPN.URL,
		Name:   resultVPN.Name,
		Status: int(resultVPN.Status),
	}

	tests := []test{
		{
			name: "valid update vpns",
			fields: fields{
				repo: func() vpn.Repo {
					m := mock_vpn.NewMockRepo(gomock.NewController(t))
					m.EXPECT().WriteVPN(repoVPN).Return(nil)
					m.EXPECT()
					return m
				}(),
				sources: func() []vpn.Source {
					m := mock_vpn.NewMockSource(gomock.NewController(t))
					m.EXPECT().
						Next().
						Return([]*vpn.OVPN{&resultVPN})
					return []vpn.Source{m}
				}(),
				timer: func() vpn.Timer {
					m := mock_vpn.NewMockTimer(gomock.NewController(t))
					m.EXPECT().Now().Return(timeWrite)
					return m
				}(),
			},
			expectedErr: nil,
		},
	}

	for _, oneTest := range tests {
		oneTest := oneTest
		t.Run(t.Name(), func(t *testing.T) {
			t.Parallel()

			app := vpn.NewApp(oneTest.fields.repo, oneTest.fields.sources, oneTest.fields.timer)
			err := app.UpdateVPN()

			require.Equal(t, err, oneTest.expectedErr)
		})
	}
}
