package vpn

import (
	"net"
	"strconv"
	"time"
)

type OVPNStatus byte

const (
	NotChecked OVPNStatus = iota
	Work
	DontWork
)

func (ovpn OVPN) GetStatus() OVPNStatus {
	return ovpn.Status
}

type Checker interface {
	Check() bool
}

const (
	pingLimit        = 500
	maxTimeoutToWait = 1000
)

func (ovpn *OVPN) Check() {
	ping := 0
	pings := ovpn.ping()
	for _, onePing := range pings {
		ping += onePing / len(pings)
	}

	if ping < pingLimit {
		ovpn.Status = Work
	} else {
		ovpn.Status = DontWork
	}
}

func (ovpn OVPN) ping() [5]int {
	ping := [5]int{}
	conn := ovpn.getConnection()

	for tryIndex := 0; tryIndex < 5; tryIndex++ {
		if conn == nil {
			ping[tryIndex] = maxTimeoutToWait

			continue
		}

		timeout := maxTimeoutToWait * time.Millisecond
		start := time.Now().UnixMilli()
		_, err := net.DialTimeout("tcp", conn.Host+":"+strconv.Itoa(conn.Port), timeout)
		end := time.Now().UnixMilli()
		if err != nil {
			ping[tryIndex] = maxTimeoutToWait
		} else {
			ping[tryIndex] = int(end - start)
		}
	}

	return ping
}
