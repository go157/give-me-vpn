package vpn

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/ffSaschaGff/vpn-checker/internal/repository"
)

type OVPN struct {
	URL    string
	Name   string
	Status OVPNStatus
}

type Connection struct {
	Host string
	Port int
}

type App interface {
	ReadRandomVPN() (OVPN, error)
	ClearOldData() error
	UpdateVPN() error
}

type Repo interface {
	ReadVPN(vpnID int) (repository.OVPN, error)
	WriteVPN(ovpn repository.OVPN) error
	ReadRandomVPN(status int) (repository.OVPN, error)
	ClearOldData(limit time.Duration) error
}

type Source interface {
	Next() []*OVPN
}

type Timer interface {
	Now() time.Time
}

type defTimer struct{}

type app struct {
	repo    Repo
	sources []Source
	timer   Timer
}

// ErrOnVPNUpdate - err on attempt to load new vpns.
var ErrOnVPNUpdate = errors.New("error on vpn update")

func NewApp(repo Repo, sources []Source, timer Timer) App {
	return app{
		repo:    repo,
		sources: sources,
		timer:   timer,
	}
}

func NewTimer() Timer {
	return defTimer{}
}

func (ovpn OVPN) String() string {
	var status string
	switch ovpn.Status {
	case Work:
		status = "Work"
	case DontWork:
		status = "Don't work"
	case NotChecked:
		status = "Not checked"
	}

	return fmt.Sprintf("%s \t %s \t %s", ovpn.Name, ovpn.URL, status)
}

func (ovpn OVPN) GetName() string {
	return ovpn.Name
}

func (ovpn OVPN) GetURL() string {
	return ovpn.URL
}

func (ovpn OVPN) getConnection() *Connection {
	// get ovpn content
	timeoutCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	req, err := http.NewRequestWithContext(timeoutCtx, http.MethodGet, ovpn.URL, nil)
	if err != nil {
		log.Println(err.Error())

		return nil
	}

	hc := http.Client{}
	res, err := hc.Do(req)
	if err != nil {
		log.Println(err.Error())

		return nil
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		log.Printf("status code error: %d %s\n", res.StatusCode, res.Status)

		return nil
	}

	// let's parse it!
	buf := new(strings.Builder)
	_, err = io.Copy(buf, res.Body)
	if err != nil {
		log.Println(err.Error())

		return nil
	}
	ovpnBody := buf.String()
	connMatch := regexp.MustCompile(`remote \d+\.\d+\.\d+\.\d+ \d+`)
	match := connMatch.Find([]byte(ovpnBody))

	if match == nil {
		return nil
	}
	matchParts := strings.Split(string(match), " ")
	port, err := strconv.Atoi(matchParts[2])
	if err != nil {
		log.Println(err.Error())

		return nil
	}

	conn := Connection{
		Host: matchParts[1],
		Port: port,
	}

	return &conn
}

func (service app) Save(ovpn OVPN) error {
	now := service.timer.Now().Unix()
	dbVPN := repository.OVPN{
		URL:    ovpn.URL,
		Name:   ovpn.Name,
		Status: int(ovpn.Status),
		TS:     now,
	}

	return service.repo.WriteVPN(dbVPN)
}

func (service app) UpdateVPN() error {
	list := []*OVPN{}
	allErrors := []string{}

	for _, source := range service.sources {
		list = append(list, source.Next()...)
	}

	errCh := make(chan error)

	go func() {
		for err := range errCh {
			if err != nil {
				allErrors = append(allErrors, err.Error())
			}
		}
	}()

	waitGroup := sync.WaitGroup{}
	for _, vpn := range list {
		waitGroup.Add(1)
		go service.saveAsyncVPN(vpn, errCh, &waitGroup)
	}
	waitGroup.Wait()
	close(errCh)

	if len(allErrors) == 0 {
		log.Println("VPNs have been updated")

		return nil
	}

	return fmt.Errorf("%w: %s", ErrOnVPNUpdate, strings.Join(allErrors, "; "))
}

func (service app) ClearOldData() error {
	limit := 86400 // 1d
	tsLimit := service.timer.Now().Add(-time.Duration(limit)).Unix()

	return service.repo.ClearOldData(time.Duration(tsLimit))
}

func (service app) ReadRandomVPN() (OVPN, error) {
	vpn := OVPN{}
	dbVPN, err := service.repo.ReadRandomVPN(int(Work))
	if err != nil {
		return vpn, err
	}

	vpn.Name = dbVPN.Name
	vpn.URL = dbVPN.URL
	vpn.Status = OVPNStatus(dbVPN.Status)

	return vpn, nil
}

func (timer defTimer) Now() time.Time {
	return time.Now()
}

func (service app) saveAsyncVPN(vpn *OVPN, errCh chan<- error, wg *sync.WaitGroup) {
	defer wg.Done()
	dbVPN := repository.OVPN{
		URL:    vpn.URL,
		Name:   vpn.Name,
		Status: int(vpn.Status),
	}
	err := service.repo.WriteVPN(dbVPN)
	errCh <- err
}
