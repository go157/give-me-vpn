package repository

import (
	"log"
	"math/rand"
	"time"

	"github.com/jmoiron/sqlx"
)

type VPNRepo struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) VPNRepo {
	return VPNRepo{
		db: db,
	}
}

type OVPN struct {
	URL    string
	Name   string
	Status int
	ID     int   `db:"id"`
	TS     int64 `db:"ts"`
}

func (repo VPNRepo) ReadVPN(vpnID int) (OVPN, error) {
	ovpn := OVPN{}

	row := repo.db.QueryRowx(`SELECT
		name,
		url,
		status
		FROM vpn
		WHERE id=$1`, vpnID)

	err := row.StructScan(&ovpn)
	if err != nil {
		return OVPN{}, err
	}

	return ovpn, nil
}

func (repo VPNRepo) WriteVPN(ovpn OVPN) error {
	_, err := repo.db.Exec(`INSERT INTO vpn (ts,status,url,name)
	VALUES ($1,$2,$3,$4)`, ovpn.TS, ovpn.Status, ovpn.URL, ovpn.Name)

	return err
}

func (repo VPNRepo) ClearOldData(limit time.Duration) error {
	tsLimit := time.Now().Add(-limit).Unix()

	_, err := repo.db.Exec(`DELETE FROM vpn
	WHERE ts < $1`, tsLimit)

	return err
}

func (repo VPNRepo) ReadRandomVPN(status int) (OVPN, error) {
	rows, err := repo.db.Queryx(`SELECT
		id AS key
		FROM vpn
		WHERE status=$1
		ORDER BY ts DESC
		LIMIT 20;`, status)
	if err != nil {
		return OVPN{}, err
	}
	defer closeRows(rows)

	vpnIDs := []int{}
	for rows.Next() {
		var vpnID int
		err = rows.Scan(&vpnID)
		if err != nil {
			return OVPN{}, err
		}

		vpnIDs = append(vpnIDs, vpnID)
	}
	rand.Seed(time.Now().UnixMicro())
	index := rand.Intn(len(vpnIDs) - 1) // #nosec

	return repo.ReadVPN(vpnIDs[index])
}

func closeRows(rows *sqlx.Rows) {
	err := rows.Close()
	if err != nil {
		log.Println(err.Error())
	}
}
