package settings

import (
	"encoding/json"
	"os"
)

// Settings - settings structure.
type Settings struct {
	DBPassword string `json:"dbPassword"`
	DBLogin    string `json:"dbLogin"`
	DBName     string `json:"dbName"`
	DBHost     string `json:"dbHost"`
}

// AppSettings - readed settings.
var AppSettings Settings

// ReadSettings - read settings from env or file.
func ReadSettings() error {
	plan, err := os.ReadFile("./settings.json")
	if err != nil {
		return readSettingsFromEnv()
	}

	return readSettingsFromFile(plan)
}

func readSettingsFromFile(plan []byte) error {
	return json.Unmarshal(plan, &AppSettings)
}

func readSettingsFromEnv() error {
	AppSettings = Settings{}
	AppSettings.DBLogin = os.Getenv("DB_LOGIN")
	AppSettings.DBName = os.Getenv("DB_NAME")
	AppSettings.DBPassword = os.Getenv("DB_PASSWORD")
	AppSettings.DBHost = os.Getenv("DB_HOST")

	return nil
}
