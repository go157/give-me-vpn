package selfapi

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ffSaschaGff/vpn-checker/internal/vpn"
)

type APIerr struct {
	Message string `json:"message"`
}

type APIvpn struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

type Handler interface {
	HandlerGetVPNGet(context *gin.Context)
}

type handler struct {
	app vpn.App
}

func NewHandler(app vpn.App) Handler {
	return handler{
		app: app,
	}
}

func RunAPI(handler Handler) {
	router := gin.Default()
	version := apiVersion()
	router.GET(version+"/getVPN", handler.HandlerGetVPNGet)
	err := router.Run()
	if err == nil {
		log.Fatalln(err.Error())
	}
}

func apiVersion() string {
	return "/api/latest"
}

func (handler handler) HandlerGetVPNGet(context *gin.Context) {
	randomVPN, err := handler.app.ReadRandomVPN()
	if err != nil {
		err := APIerr{
			Message: err.Error(),
		}

		context.JSON(http.StatusInternalServerError, err)

		return
	}

	vpn := APIvpn{
		Name: randomVPN.GetName(),
		URL:  randomVPN.GetURL(),
	}

	context.JSON(http.StatusOK, vpn)
}
