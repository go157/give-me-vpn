### Сервис сбора VPN

Реализует сервис сбора рабочих VPN и отдает их по запросу

## Поддерживаемые источники

На текущий момент загружает OVPN файлы с
- https://www.freeopenvpn.org

## API сервиса

- **/api/latest/getVPN** - отдает один случайный файл подключения
