-- +goose Up 
-- +goose NO TRANSACTION
CREATE TABLE IF NOT EXISTS vpn(
    id SERIAL PRIMARY KEY,
    ts BIGINT,
    status INT,
    url TEXT,
    name TEXT
);
-- +goose Down 
-- +goose NO TRANSACTION
DROP TABLE vpn;
