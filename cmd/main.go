package main

import (
	"log"
	"time"

	"gitlab.com/ffSaschaGff/vpn-checker/internal/dbprovider"
	"gitlab.com/ffSaschaGff/vpn-checker/internal/repository"
	"gitlab.com/ffSaschaGff/vpn-checker/internal/selfapi"
	"gitlab.com/ffSaschaGff/vpn-checker/internal/settings"
	"gitlab.com/ffSaschaGff/vpn-checker/internal/vpn"
)

func main() {
	log.SetFlags(log.Lshortfile)

	err := settings.ReadSettings()
	if err != nil {
		log.Fatalln(err.Error())
	}

	db := dbprovider.NewConn()
	repo := repository.New(db)
	sources := []vpn.Source{}
	sources = append(sources, vpn.NewFreeOpenVpn())
	sources = append(sources, vpn.NewIPSpeedVPN())
	app := vpn.NewApp(repo, sources, vpn.NewTimer())

	day := 24
	runInBg(app.UpdateVPN, time.Hour)
	runInBg(app.ClearOldData, time.Hour*time.Duration(day))

	handlers := selfapi.NewHandler(app)
	selfapi.RunAPI(handlers)
}

func runInBg(action func() error, delay time.Duration) {
	go func() {
		for {
			err := action()
			if err != nil {
				log.Println(err)
			}
			time.Sleep(delay)
		}
	}()
}
